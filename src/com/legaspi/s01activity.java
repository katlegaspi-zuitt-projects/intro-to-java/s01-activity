package com.legaspi;

public class s01activity {

    public static void main(String[] args) {
        System.out.println("S01 Activity");

        int playerNumber = 1;

        double playerStrength = 99.99;

        char team = 'A';

        boolean multiplayer = false;

        System.out.println("\nPlayer: " + playerNumber + "\nStrength: " + playerStrength + "\nTeam: " + team + "\nMultiplayer mode: " + multiplayer);
    }
}
